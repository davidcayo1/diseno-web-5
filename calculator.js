const screen = document.getElementById("calculator-screen"),
  keys = document.getElementById("calculator-keys");

  // para poder escribir el tipo de operación
  let operationStatus = false;
  let number1, typeOperation;

  screen.textContent = '0';

const calculator = () => {

  // si keys no existe que termine la función
  if (!keys) {
    return
  }

  keys.addEventListener('click', (e) => {
    const t = e.target, // obtenemos el valor del del button
          d = t.dataset; // obtenemos el valor del data-

          console.log(d)
    //  Verificar si se pulsón un número
    if (d.number) {
      writeScreen(d.number);
    }

    // Verificar si se pulso una operación matemática
    if (d.math) {
      // t, es el valor del button
      getOperation(t, d.math);
    }

    // detectar si s pulsó otra operación
    if (d.operation) {
      runOperation(d.operation);
    }
  })
}

// para escribir en la pantalla
const writeScreen = (number) => {
  screen.textContent === '0' || operationStatus === true
    ? screen.textContent = number
    // si es que hay un punto, pero no ha habido un punto anteriormente
    : number === '.' && !screen.textContent.includes('.')
      // escribe ese numero es decir el punto
      ? screen.textContent += number
      // y si no que el número sea distinto del punto
      : number !== '.'
        ? screen.textContent += number
        : null

    operationStatus = false;
}

// para realizar una operación
const getOperation = (element, operation) => {
  operationStatus = true;

  // primero guardo el numero que estaba en pantalla
  number1 = Number(screen.textContent);
  // esto necesito para saber el tipo de operacion que realizare
  typeOperation = operation;
  // reemplazo el contenido en pantalla por el simbolo de la operación
  screen.textContent = element.textContent;

}


const runOperation = (operation) => {
 
    const getResult = (number1,typeOperation) => {
      const number2 = Number(screen.textContent);
      let result = 0;

      switch (typeOperation) {
        case 'suma':
          result = number1 + number2;
          break;

        case 'resta':
          result = number1 - number2;
          break;

        case 'multiplicacion':
          result = number1 * number2;
          break;

        case 'division':
          result = number1 / number2;
          break;
          
          default:
            break;
      }

      
      result === Infinity
        ? screen.textContent = 'Error'
        : screen.textContent = result
    }

    operation === "clear"
      ? screen.textContent = "0"
      : getResult(number1, typeOperation);

    operationStatus = true;
}

calculator();